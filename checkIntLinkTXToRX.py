#!/usr/bin/env python

import os
import sys
import argparse, glob

def merge_two_dicts( x, y ):
  z = x.copy()
  z.update(y)
  return z


def getTXLaneToChannelSlot( board, lane ):

  lane_to_ch = {0:2, 1:4, 2:5, 3:6, 4:7, 5:8, 6:9, 7:10, 8:11, 9:12, 10:0, 11:0, 12:3, 13:4, 14:5, 15:6, 16:7, 17:8, 18:9, 19:10, 20:11, 21:12, 22:0}

  if lane_to_ch[lane] > 9 or lane_to_ch[lane] < 3:
    return -1

  slot3 = {3: '4-3', 4: '5-3', 5: '6-3', 6: '7-3', 7: '8-3', 8: '9-3' , 9: '10-3'}
  slot4 = {3: '3-3', 4: '5-4', 5: '6-4', 6: '7-4', 7: '8-4', 8: '9-4' , 9: '10-4'}
  slot5 = {3: '3-4', 4: '4-4', 5: '6-5', 6: '7-5', 7: '8-5', 8: '9-5' , 9: '10-5'}
  slot6 = {3: '3-5', 4: '4-5', 5: '5-5', 6: '7-6', 7: '8-6', 8: '9-6' , 9: '10-6'}
  slot7 = {3: '3-6', 4: '4-6', 5: '5-6', 6: '6-6', 7: '8-7', 8: '9-7' , 9: '10-7'}
  slot8 = {3: '3-7', 4: '4-7', 5: '5-7', 6: '6-7', 7: '7-7', 8: '9-8' , 9: '10-8'}
  slot9 = {3: '3-8', 4: '4-8', 5: '5-8', 6: '6-8', 7: '7-8', 8: '8-8' , 9: '10-9'}
  slot10 = {3: '3-9', 4: '4-9', 5: '5-9', 6: '6-9', 7: '7-9', 8: '8-9' , 9: '9-9'}
  
  if board == '3':
    slot_channel = slot3[lane_to_ch[lane]]
  elif board == '4':
    slot_channel = slot4[lane_to_ch[lane]]
  elif board == '5':
    slot_channel = slot5[lane_to_ch[lane]]
  elif board == '6':
    slot_channel = slot6[lane_to_ch[lane]]
  elif board == '7':
    slot_channel = slot7[lane_to_ch[lane]]
  elif board == '8':
    slot_channel = slot8[lane_to_ch[lane]]
  elif board == '9': 
    slot_channel = slot9[lane_to_ch[lane]]
  elif board == '10':
    slot_channel = slot10[lane_to_ch[lane]]
    
  return slot_channel


def getChannelToRXLane( channel ):
  lanes = [0, 0]
  ch_to_lanes = {'3': [1,17],
                 '4': [2,18],
                 '5': [3,19],
                 '6': [5,21],
                 '7': [6,22],
                 '8': [7,23],
                 '9': [9,25],
                 '10': [10,26],
                 '11': [11,27],
                 '12': [13,29]}
  lanes = ch_to_lanes[ channel ]
  return lanes

def getDFLinksBlock( spyfile_path ):
  links_text = []
  with open( spyfile_path, "r" ) as f:
    writing = False
    lc = 0
    for line in f:
      lc += 1
      if line.startswith("        |        FMCIN"): writing = True
      if line.startswith("[Device:")              : writing = False
      if writing == True:
        links_text.append(line)
  return links_text
    
if __name__=='__main__':

 parser = argparse.ArgumentParser(description="Parse IMDF spybuffer files (assumed to be all files for a particular run) and trace IntLink TX backpressure source.")
 parser.add_argument("IMDF_spyfile_dir")

 args = parser.parse_args()

 spyfile_dir = args.IMDF_spyfile_dir
 spyfiles = glob.glob( spyfile_dir+"*.dat" )

 if len( spyfiles ) == 0:
   print "%s globbed to no files!"%(spyfile_dir+"ISIMAllSpy*.dat")
   quit()
   
 for spyfile in spyfiles:
   boardnames = set()
   if not "DF-" in spyfile:
     print "Couldn't parse boardname from spy file %s"%spyfile
     continue
   boardname = spyfile[spyfile.find("DF-"):spyfile.find("DF-")+7]
   
   boardnames.add(boardname)

   for board in boardnames:
     boardnum = board[board.find("DF-")+6:]
     links_block = getDFLinksBlock( spyfile )
     
     line_count=0
     for line in links_block:
       if line.startswith("        |        FMCIN"): continue
       line_count += 1       
       if 'X'  in line.split(' |')[13]:
         lane = line_count-1
         print "*** Found BP in IntLinkTX for lane %i on board %s ***"%(lane,board)

         slot_ch = getTXLaneToChannelSlot( boardnum, lane )
         if slot_ch == -1: 
           continue

         print "TX lane %i sends data to slot-channel: "%(lane)+slot_ch
         
         slot = slot_ch[0:slot_ch.find("-")]
         channel = slot_ch[slot_ch.find("-")+1]
         
         rxlanes = getChannelToRXLane(channel)
         print "%s TX lane %i is sending data to DF-4-0%s RX lanes"%(board,lane,slot),
         print rxlanes
         print ""
        
